export interface IData {
    "id": number,
    "position": string,
    "company": string,
    "dateStart": string,
    "dateEnd": string,
    "stack": string[],
    "experience": string[]
}