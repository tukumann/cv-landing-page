import { Component } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { IData } from './interfaces/IData';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public title = 'cv-landing';
  public data: IData[] = [];

  constructor(private http: HttpClient) {
    this.http.get<any>('assets/db.json').subscribe(data => {
      this.data = data.data; 
    });
  }
}
